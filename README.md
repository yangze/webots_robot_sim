# webots_robot_sim



## Overview

This is simple cleanbot model created by Webots 2022a.

![world_sample_image](doc/world_sample_image.jpg)

## Features

- [x] stable physical modeling
- [x] 2D-Lidar
- [ ] 

## Usage

### Build the simple controller

```shell
cd webots_robot_sim/controllers/bin_simple_move
mkdir build
cd build
cmake .. & make
```

### Load world in Webots

- In Webots use **File** -> **Open World** to open the world file `lidar_robot.wbt`
- 

## Support


## Roadmap
## License
Apache License V2.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

