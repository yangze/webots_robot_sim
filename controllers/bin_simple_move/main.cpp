#include <iostream>
#include <webots/Lidar.hpp>
#include <webots/Motor.hpp>
#include <webots/Robot.hpp>

using namespace webots;
using namespace std;

#define TIME_STEP 64
#define MAX_SPEED_RAD_S 12.56

int main(int argc, char **argv) {
  Robot *robot = new Robot();

  // motor initialize
  Motor *leftMotor = robot->getMotor("motor_l");
  Motor *rightMotor = robot->getMotor("motor_r");

  if (leftMotor) {
    leftMotor->setVelocity(MAX_SPEED_RAD_S);
    cout << "left motor get success" << leftMotor->getVelocity() << endl;
  }

  if (rightMotor) {
    rightMotor->setVelocity(MAX_SPEED_RAD_S);
    cout << "right motor get success" << rightMotor->getVelocity() << endl;
  }

  // lidar initialize
  Lidar lidar("LDS-01");
  lidar.enable(TIME_STEP);
  lidar.enablePointCloud();

  //
  leftMotor->setPosition(INFINITY);
  rightMotor->setPosition(INFINITY);

  // set up the motor speeds at 10% of the MAX_SPEED.
  leftMotor->setVelocity(0.1 * MAX_SPEED_RAD_S);
  rightMotor->setVelocity(0.1 * MAX_SPEED_RAD_S);

  while (robot->step(TIME_STEP) != -1)
    ;


  delete robot;

  return 0;
}